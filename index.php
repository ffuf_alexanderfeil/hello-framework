<?php

require 'vendor/autoload.php';

use rueckgrat\debug\Debugger;
use rueckgrat\Application;

ob_start('ob_gzhandler');

require 'bootstrap.php';

Debugger::appStart();

new Application();

Debugger::appEnd();
ob_end_flush();
?>
