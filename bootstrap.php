<?php

require 'config/config.php';

function clsLoader($className) {
    $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    require ROOT_DIR . $classPath . '.php';
}

spl_autoload_register('clsLoader');
