<?php

// DB-Settings
define('DB_DSN', 'mysql:host=127.0.0.1;dbname=mvc');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

/**
 * Absolute path of root dir
 */
define('ROOT_DIR', getcwd() . DIRECTORY_SEPARATOR);
/**
 * Debugmode enabeld?
 */
define('DEBUG_MODE', TRUE);
/**
 * The default controller class
 */
define('DEFAULT_CONTROLLER', 'Example');
/**
 * Session handler
 */
define('SESSION_HANDLER_CLASS_NAME', 'PHPSession');
/**
 * Cache handler
 */
define('CACHE_HANDLER_CLASS_NAME', 'FileCache');
/**
 * Cache directory
 */
define('CACHE_DIR', ROOT_DIR . 'cache' . DIRECTORY_SEPARATOR);
/**
 * Lifetime of a cached file
 */
define('CACHE_LIFETIME', 86400);
/**
 * Absolute path to the templates dir
 */
define('FASTVIEW_DEFAULT_TPL_PATH', ROOT_DIR . 'app' . DIRECTORY_SEPARATOR . 'tpl' . DIRECTORY_SEPARATOR);
/**
 * Absolute path to the compile dir
 */
define('FASTVIEW_COMP_TPL_DIR', CACHE_DIR . 'tpl' . DIRECTORY_SEPARATOR);
